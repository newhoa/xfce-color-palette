# Xfce Color Palette

Xfce color palette in SVG and GPL formats for working with Xfce icons in Inkscape and Gimp.

For use in Inkscape and Gimp, place the .gpl file in:

- ~/.config/inkscape/palettes/
- ~/.config/GIMP/GIMP_VERSION/palettes/


![](https://gitlab.xfce.org/newhoa/xfce-color-palette/-/raw/master/xfce-color-palette.svg)

| Color | Hexadecimal | RGB | Name |
| :--: | :--: | :--: | :--: |
| ![](color-chips/midnight-1.png) | #61757f | (97, 117, 127) | Midnight 1 |
| ![](color-chips/midnight-2.png) | #485e6b | (72, 94, 107) | Midnight 2 |
| ![](color-chips/midnight-3.png) | #344958 | (52, 73, 88) | Midnight 3 |
| ![](color-chips/midnight-4.png) | #263742 | (38, 55, 66) | Midnight 4 |
| ![](color-chips/midnight-5.png) | #070c0f | (7, 12, 15) | Midnight 5 |
| ![](color-chips/grey-1.png) | #f1f3f5 | (241, 243, 245) | Grey 1 | 
| ![](color-chips/grey-2.png) | #d2d8dc | (210, 216, 220) | Grey 2 | 
| ![](color-chips/grey-3.png) | #bcc5ca | (188, 197, 202) | Grey 3 | 
| ![](color-chips/grey-4.png) | #a7b0b7 | (167, 176, 183) | Grey 4 | 
| ![](color-chips/grey-5.png) | #758087 | (117, 128, 135) | Grey 5 | 
| ![](color-chips/blue-1.png) | #c1ebf7 | (193, 235, 247) | Blue 1 | 
| ![](color-chips/blue-2.png) | #6acfec | (106, 207, 236) | Blue 2 | 
| ![](color-chips/blue-3.png) | #00aade | (0, 170, 222) | Blue 3 | 
| ![](color-chips/blue-4.png) | #006888 | (0, 104, 136) | Blue 4 | 
| ![](color-chips/blue-5.png) | #003445 | (0, 52, 69) | Blue 5 | 
| ![](color-chips/green-1.png) | #d7fdb2 | (215, 253, 178) | Green 1 | 
| ![](color-chips/green-2.png) | #aef766 | (174, 247, 102) | Green 2 | 
| ![](color-chips/green-3.png) | #6acc0a | (106, 204, 10) | Green 3 | 
| ![](color-chips/green-4.png) | #52a302 | (82, 163, 2) | Green 4 | 
| ![](color-chips/green-5.png) | #3c7902 | (60, 121, 2) | Green 5 | 
| ![](color-chips/yellow-1.png) | #fcf4c0 | (252, 244, 192) | Yellow 1 | 
| ![](color-chips/yellow-2.png) | #fbeb83 | (251, 235, 131) | Yellow 2 | 
| ![](color-chips/yellow-3.png) | #f9da02 | (249, 218, 2) | Yellow 3 | 
| ![](color-chips/yellow-4.png) | #dcb102 | (220, 177, 2) | Yellow 4 | 
| ![](color-chips/yellow-5.png) | #ad7702 | (173, 119, 2) | Yellow 5 | 
| ![](color-chips/red-orange-1.png) | #f3b6a6 | (243, 182, 166) | Red-Orange 1 | 
| ![](color-chips/red-orange-2.png) | #ec866a | (236, 134, 106) | Red-Orange 2 | 
| ![](color-chips/red-orange-3.png) | #e6461d | (230, 70, 29) | Red-Orange 3 | 
| ![](color-chips/red-orange-4.png) | #c02700 | (192, 39, 0) | Red-Orange 4 | 
| ![](color-chips/red-orange-5.png) | #851b00 | (133, 27, 0) | Red-Orange 5 | 
| ![](color-chips/purple-1.png) | #d4ade2 | (212, 173, 226) | Purple 1 | 
| ![](color-chips/purple-2.png) | #b97ace | (185, 122, 206) | Purple 2 | 
| ![](color-chips/purple-3.png) | #9c41bf | (156, 65, 191) | Purple 3 | 
| ![](color-chips/purple-4.png) | #753390 | (117, 51, 144) | Purple 4 | 
| ![](color-chips/purple-5.png) | #471d57 | (71, 29, 87) | Purple 5 | 
| ![](color-chips/brown-1.png) | #c6a798 | (198, 167, 152) | Brown 1 | 
| ![](color-chips/brown-2.png) | #a17a69 | (161, 122, 105) | Brown 2 | 
| ![](color-chips/brown-3.png) | #79503c | (121, 80, 60) | Brown 3 | 
| ![](color-chips/brown-4.png) | #603f2f | (96, 63, 47) | Brown 4 | 
| ![](color-chips/brown-5.png) | #4a3025 | (74, 48, 37) | Brown 5 | 
